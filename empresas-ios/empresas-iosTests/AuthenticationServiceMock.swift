//
//  AuthenticationServiceMock.swift
//  empresas-iosTests
//
//  Created by Edno Fedulo on 10/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation
@testable import empresas_ios

class AuthenticationServiceMock:AuthenticationServiceProtocol {
    
    var fail:Bool
    
    init(fail:Bool) {
        self.fail = fail
    }
    
    func login(email: String, password: String, success: @escaping (Credentials?) -> Void, error: @escaping ([String?]?) -> Void) {
        if fail {
            error(["erro"])
        } else {
            success(Credentials(client: "", uid: "", accessToken: ""))
        }
    }
}
