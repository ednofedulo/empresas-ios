//
//  LoginViewControllerTest.swift
//  empresas-iosTests
//
//  Created by Edno Fedulo on 10/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import XCTest
@testable import empresas_ios

class LoginViewControllerTest: XCTestCase {
    
    var loginViewController:LoginViewController?
    
    override func setUp() {
        self.loginViewController = UIApplication.shared.keyWindow?.rootViewController as! LoginViewController
    }
    
    override func tearDown() {
        self.loginViewController = nil
    }
    
    func testDidTapOnLoginButtonEmptyForm() {
        self.loginViewController?.loginTextField.textField.text = ""
        self.loginViewController?.passwordTextField.textField.text = ""
        self.loginViewController?.didTapOnLoginButton()
        XCTAssert(self.loginViewController?.loginTextField.infoLabel.text == "Email obrigatório")
    }
    
    func testDidTapOnLoginButtonInvalidCredentials(){
        self.loginViewController?.authenticationService = AuthenticationServiceMock(fail: true)
        self.loginViewController?.loginTextField.textField.text = "qwe"
        self.loginViewController?.passwordTextField.textField.text = "eqw"
        self.loginViewController?.didTapOnLoginButton()
        
        XCTAssert(self.loginViewController?.passwordTextField.infoLabel.text != nil && self.loginViewController?.passwordTextField.infoLabel.text?.isEmpty == false)
    }
    
    func testDidTapOnLoginButtonSuccess(){
        self.loginViewController?.authenticationService = AuthenticationServiceMock(fail: false)
        self.loginViewController?.loginTextField.textField.text = "eqwewq"
        self.loginViewController?.passwordTextField.textField.text = "eqweqwe"
        self.loginViewController?.didTapOnLoginButton()
        
        let exp = expectation(description: "login_wait")
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            exp.fulfill()
        }
        waitForExpectations(timeout: 2, handler: nil)
        XCTAssert(UIApplication.shared.keyWindow?.rootViewController?.title == "Empresas")
    }
    
}
