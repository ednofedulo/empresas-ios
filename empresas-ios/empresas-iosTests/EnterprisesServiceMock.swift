//
//  EnterprisesServiceMock.swift
//  empresas-iosTests
//
//  Created by Edno Fedulo on 10/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation
@testable import empresas_ios

class EnterprisesServiceMock : EnterprisesServiceProtocol {
    
    var fail:Bool
    
    init(fail:Bool) {
        self.fail = fail
    }
    
    func searchEnterprises(by name: String, withCredentials credentials: Credentials, success: @escaping (EnterprisesResponse?) -> Void, error: @escaping () -> Void) {
        if self.fail {
            error()
        } else {
            var response = EnterprisesResponse()
            response.enterprises = [
                Enterprise(enterpriseName: "Test enterprise 1", description: "Test enterprise 1 description lorem ipsum dolor sit amet"),
                Enterprise(enterpriseName: "Test enterprise 2", description: "Test enterprise 2 description lorem ipsum dolor sit amet"),
                Enterprise(enterpriseName: "Test enterprise 3", description: "Test enterprise 3 description lorem ipsum dolor sit amet"),
                Enterprise(enterpriseName: "Test enterprise 4", description: "Test enterprise 4 description lorem ipsum dolor sit amet")
            ]
            
            success(response)
        }
    }
    
    
}
