//
//  empresas_iosUITests.swift
//  empresas-iosUITests
//
//  Created by Edno Fedulo on 08/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import XCTest
@testable import empresas_ios

class empresas_iosUITests: XCTestCase {
    
    func testLoginFormDisplayRequiredFieldsErrorMessages() throws {
        let app = XCUIApplication()
        app.launch()
        let entrarStaticText = app/*@START_MENU_TOKEN@*/.staticTexts["ENTRAR"]/*[[".buttons[\"ENTRAR\"].staticTexts[\"ENTRAR\"]",".staticTexts[\"ENTRAR\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        entrarStaticText.tap()
        XCTAssert(app.staticTexts["Email obrigatório"].exists)
        let emailTextField = app.textFields["email"]
        emailTextField.tap()
        emailTextField.typeText("email")
        entrarStaticText.tap()
        XCTAssert(app.staticTexts["Senha obrigatória"].exists)
    }
    
    func testLoadingViewIsDisplayingOnLogin() {
        let app = XCUIApplication()
        app.launch()
        let entrarStaticText = app/*@START_MENU_TOKEN@*/.staticTexts["ENTRAR"]/*[[".buttons[\"ENTRAR\"].staticTexts[\"ENTRAR\"]",".staticTexts[\"ENTRAR\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        
        let emailTextField = app.textFields["email"]
        let senhaTextField = app.secureTextFields["senha"]
        
        emailTextField.tap()
        emailTextField.typeText("email")
        
        senhaTextField.tap()
        senhaTextField.typeText("senha")
        
        entrarStaticText.tap()
        XCTAssert(app.otherElements["loadingView"].exists)
    }
    
    func testErrorMessagesStartHidden() {
        let app = XCUIApplication()
        app.launch()
        
        XCTAssert(app.staticTexts["Email obrigatório"].exists == false)
        XCTAssert(app.staticTexts["Senha obrigatória"].exists == false)
    }
}
