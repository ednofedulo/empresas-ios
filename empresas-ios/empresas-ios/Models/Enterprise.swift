//
//  Enterprise.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 09/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation

struct EnterprisesResponse:Decodable {
    var enterprises: [Enterprise]?
}

struct Enterprise: Decodable {
    var id:Int?
    var enterpriseName:String?
    var description:String?
    var emailEnterprise:String?
    var facebook:String?
    var twitter:String?
    var linkedin:String?
    var phone:String?
    var ownEnterprise:Bool?
    var photo:String?
    var value:Float?
    var shares:Int?
    var sharePrice:Float?
    var city:String?
    var country:String?
    var enterpriseType:EnterpriseType?
}
