//
//  ResponseError.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 09/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation

struct ResponseError : Decodable {
    var errors:[String]?
    var success:Bool?
}
