//
//  Credentials.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 09/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation

struct Credentials {
    var client:String?
    var uid:String?
    var accessToken:String?
}
