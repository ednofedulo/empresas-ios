//
//  APIDefaults.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 09/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation

struct APIDefaults {
    static var baseUrl = "https://empresas.ioasys.com.br/api"
    static var apiVersion = "v1"
    static var url = "\(baseUrl)/\(apiVersion)/"
}
