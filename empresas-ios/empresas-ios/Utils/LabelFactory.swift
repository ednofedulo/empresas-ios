//
//  LabelFactory.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 10/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation
import UIKit

class LabelFactory {
    
    static func generateLabel(_ text: String, style: UIFont.TextStyle = .body, color: UIColor = .label) -> UILabel {
        let label = CustomLabel(withTextStyle: style)
        label.text = text
        label.textColor = color
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }
}
