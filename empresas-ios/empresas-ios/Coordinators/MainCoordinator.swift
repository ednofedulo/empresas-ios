//
//  MainCoordinator.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 08/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator {
    
    let window: UIWindow
    
    var homeNavigationController:UINavigationController?

    var credentials:Credentials?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    var displayingView: UIViewController? {
        get {
            return window.rootViewController
        }
        set {
           self.window.rootViewController = newValue
            UIView.transition(with: self.window, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion: nil)
        }
    }
    
    func start(){
        if let _ = credentials {
            showHome()
        } else {
            showLogin()
        }
    }
    
    func showHome(){
        let controller = HomeViewController()
        controller.mainCoordinator = self
        homeNavigationController = UINavigationController(rootViewController: controller)
        self.displayingView = homeNavigationController
    }
    
    private func showLogin(){
        let controller = LoginViewController()
        controller.mainCoordinator = self
        self.displayingView = controller
    }
}
