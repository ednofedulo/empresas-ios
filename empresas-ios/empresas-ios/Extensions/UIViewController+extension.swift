//
//  UIViewController+extension.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 10/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func sceneDelegate() -> SceneDelegate? {
        guard let window = self.view.window, let windowScene = window.windowScene, let sceneDelegate = windowScene.delegate as? SceneDelegate else { return nil }
        
        return sceneDelegate
    }
    
    func showLoading(over view:UIView? = nil){
        let container = UIView()
        container.tag = 10000
        container.backgroundColor = .secondarySystemFill
        let loadingContainer = LoadingView()
        
        container.addSubview(loadingContainer)
        
        if let view = view {
            view.addSubview(container)
            container.bounds = view.bounds
            container.center = view.center
        } else {
            self.view.addSubview(container)
            container.bounds = self.view.bounds
            container.center = self.view.center
        }
        
        loadingContainer.center = container.center
        loadingContainer.bounds = CGRect(origin: .zero, size: CGSize(width: 100, height: 100))

        loadingContainer.alpha = 0
        loadingContainer.startRotating()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
            loadingContainer.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            loadingContainer.alpha = 1
        })
    }
    
    func hideLoading(from view:UIView? = nil){
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        var loadingView:UIView? = nil
        
        loadingView = view == nil ? self.view.viewWithTag(10000) : view?.viewWithTag(10000)
        
        if let loadingView = loadingView {
            UIView.transition(with: loadingView, duration: 0.1, options: [.transitionCrossDissolve, .curveEaseOut], animations: {
                loadingView.alpha = 0
            }, completion: {
                (completed) in
                loadingView.removeFromSuperview()
            })
        }
    }
}
