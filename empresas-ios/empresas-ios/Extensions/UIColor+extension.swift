//
//  UIColor+extension.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 08/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import UIKit

extension UIColor {
    
    open class var rosa:UIColor {
        return UIColor(red: 0.88, green: 0.12, blue: 0.41, alpha: 1.00)
    }
}
