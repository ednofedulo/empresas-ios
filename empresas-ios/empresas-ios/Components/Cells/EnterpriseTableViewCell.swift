//
//  EnterpriseTableViewCell.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 09/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    
    var enterpriseTitleView:EnterpriseTitleView?
    
    var enterprise:Enterprise? {
        didSet {
            if let enterprise = enterprise {
                enterpriseTitleView?.titleLabel.text = enterprise.enterpriseName
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        self.enterpriseTitleView = EnterpriseTitleView()
        self.enterpriseTitleView?.translatesAutoresizingMaskIntoConstraints = false
        
        addViews()
        setupConstraints()
        setupViews()
    }
    
    private func addViews(){
        guard let enterpriseTitleView = enterpriseTitleView else { return }
        self.contentView.addSubview(enterpriseTitleView)
    }
    
    private func setupConstraints(){
        guard let enterpriseTitleView = enterpriseTitleView else { return }
        
        NSLayoutConstraint.activate([
            enterpriseTitleView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 15),
            enterpriseTitleView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 5),
            enterpriseTitleView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -15),
            enterpriseTitleView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5)
        ])

    }
    
    private func setupViews(){
        guard let enterpriseTitleView = enterpriseTitleView else { return }
        
        enterpriseTitleView.coloredBackgroundView.layer.cornerRadius = 10
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
