//
//  SearchHeaderView.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 09/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import UIKit

protocol SearchHeaderViewDelegate:AnyObject {
    func didBeginEditing()
    func search(searchText:String?)
}

class SearchHeaderView: UIView {
    
    lazy var searchTextField: UITextField = {
        let t = UITextField()
        t.layer.cornerRadius = 5
        t.translatesAutoresizingMaskIntoConstraints = false
        t.heightAnchor.constraint(equalToConstant: 44).isActive = true
        t.backgroundColor = .secondarySystemBackground
        t.placeholder = "Busque por empresas"
        t.delegate = self
        t.returnKeyType = .search
        t.leftViewMode = .always
        let imageView = UIImageView(image: UIImage(systemName: "magnifyingglass")?.withRenderingMode(.alwaysTemplate))
        imageView.tintColor = .secondaryLabel
        t.leftView = imageView
        return t
    }()
    
    lazy var backgroundImageView:UIImageView = {
        let i = UIImageView(image: UIImage(named: "top_image"))
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    weak var delegate:SearchHeaderViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubViews()
        setupConstraints()
        setupView()
    }
    
    private func addSubViews(){
        self.addSubview(backgroundImageView)
        self.addSubview(searchTextField)
    }
    
    private func setupConstraints(){
        NSLayoutConstraint.activate([
            searchTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            searchTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            searchTextField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10)
        ])
        
        NSLayoutConstraint.activate([
            backgroundImageView.bottomAnchor.constraint(equalTo: searchTextField.centerYAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            backgroundImageView.topAnchor.constraint(equalTo: self.topAnchor)
        ])
    }
    
    private func setupView(){
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension SearchHeaderView : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let delegate = self.delegate else { return }
        delegate.didBeginEditing()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let delegate = self.delegate else { return false }
        delegate.search(searchText: textField.text)
        return false
    }
}
