//
//  EnterpriseTitleView.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 10/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation
import UIKit

class EnterpriseTitleView : UIView {
    
    lazy var coloredBackgroundView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .systemBlue
        return v
    }()
    
    lazy var titleLabel: CustomLabel = {
        let l = CustomLabel(withTextStyle: .title2)
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textAlignment = .center
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        l.textColor = .white
        return l
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        
        addViews()
        setupConstraints()
        setupViews()
    }
    
    private func addViews(){
        self.addSubview(coloredBackgroundView)
        self.addSubview(titleLabel)
    }
    
    private func setupConstraints(){
        NSLayoutConstraint.activate([
            self.coloredBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.coloredBackgroundView.topAnchor.constraint(equalTo: self.topAnchor),
            self.coloredBackgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.coloredBackgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            self.titleLabel.leadingAnchor.constraint(equalTo: self.coloredBackgroundView.leadingAnchor, constant: 5),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.coloredBackgroundView.trailingAnchor, constant: -5),
            self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 44),
            self.bottomAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 44)
        ])
    }
    
    private func setupViews(){
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
