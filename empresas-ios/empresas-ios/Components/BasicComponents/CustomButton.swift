//
//  CustomButton.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 08/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    private var didSetupShadow:Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentEdgeInsets = UIEdgeInsets(top: 15, left: 10, bottom: 15, right: 10)
        self.layer.cornerRadius = 5
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
