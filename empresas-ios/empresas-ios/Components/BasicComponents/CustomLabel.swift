//
//  CustomLabel.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 08/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {
    
    init(withTextStyle style: UIFont.TextStyle) {
        super.init(frame: CGRect.zero)
        self.font = UIFont.preferredFont(forTextStyle: style)
        commonInit()
    }
    
    init(){
        super.init(frame: CGRect.zero)
        self.font = UIFont.preferredFont(forTextStyle: .body)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    private func commonInit(){
        self.adjustsFontForContentSizeCategory = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
