//
//  LoadingView.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 10/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    lazy var outerLoadingEllipse: UIImageView = {
        let i = UIImageView(image: UIImage(named: "loading_ellipse"))
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    lazy var innerLoadingEllipse: UIImageView = {
        let i = UIImageView(image: UIImage(named: "loading_ellipse"))
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.accessibilityIdentifier = "loadingView"
        addSubViews()
        setupConstraints()
    }
    
    private func addSubViews(){
        self.addSubview(outerLoadingEllipse)
        self.addSubview(innerLoadingEllipse)
    }
    
    private func setupConstraints(){
        NSLayoutConstraint.activate([
            self.outerLoadingEllipse.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.outerLoadingEllipse.topAnchor.constraint(equalTo: self.topAnchor),
            self.outerLoadingEllipse.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.outerLoadingEllipse.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            self.innerLoadingEllipse.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.innerLoadingEllipse.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.innerLoadingEllipse.widthAnchor.constraint(equalTo: outerLoadingEllipse.widthAnchor, multiplier: 0.5),
            self.innerLoadingEllipse.heightAnchor.constraint(equalTo: outerLoadingEllipse.heightAnchor, multiplier: 0.5)
        ])
        
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 100),
            self.widthAnchor.constraint(equalToConstant: 100)
        ])
    }
    
    func startRotating(){
        outerLoadingEllipse.rotate()
        innerLoadingEllipse.rotate(inverse: true)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
