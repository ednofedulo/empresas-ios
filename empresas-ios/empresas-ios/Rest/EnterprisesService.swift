//
//  EnterprisesRest.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 10/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation
import Alamofire

protocol EnterprisesServiceProtocol:Any {
    func searchEnterprises(by name:String, withCredentials credentials: Credentials, success: @escaping (EnterprisesResponse?) -> Void, error: @escaping () -> Void)
}

class EnterprisesService : EnterprisesServiceProtocol {
    
    func searchEnterprises(by name:String, withCredentials credentials: Credentials, success: @escaping (EnterprisesResponse?) -> Void, error: @escaping () -> Void) {
        
        guard let uid = credentials.uid, let client = credentials.client, let accessToken = credentials.accessToken else { return }
        
        let headers: HTTPHeaders = [
            .init(name: "uid", value: uid),
            .init(name: "client", value: client),
            .init(name: "access-token", value: accessToken),
        ]
        
        let url = "\(APIDefaults.url)/enterprises?name=\(name)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        AF.request(url!, method: .get, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success:
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                if let jsonData = response.data, let enterprises = try? decoder.decode(EnterprisesResponse.self, from: jsonData) {
                    success(enterprises)
                }
            case .failure:
                error()
            }
        }
    }
}
