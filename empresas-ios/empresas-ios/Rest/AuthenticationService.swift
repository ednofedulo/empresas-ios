//
//  AuthenticationRest.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 10/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import Foundation
import Alamofire

protocol AuthenticationServiceProtocol: Any {
    func login(email:String, password:String, success: @escaping (Credentials?) -> Void, error: @escaping ([String?]?) -> Void)
}

class AuthenticationService:AuthenticationServiceProtocol {
    func login(email:String, password:String, success: @escaping (Credentials?) -> Void, error: @escaping ([String?]?) -> Void) {
        
        let url = "\(APIDefaults.url)/users/auth/sign_in"
        
        let parameters = [
            "email": email,
            "password": password
        ]
        
        AF.request(url, method: .post, parameters: parameters).responseJSON { (response) in
            switch response.result {
            case .success:
                if let jsonData = response.data, let responseError = try? JSONDecoder().decode(ResponseError.self, from: jsonData), let errors = responseError.errors  {
                    error(errors);
                    return
                }
                
                guard let response = response.response else { return }
                let headers = response.headers
                guard let uid = headers.value(for: "uid") else { return }
                guard let accessToken = headers.value(for: "access-token") else { return }
                guard let client = headers.value(for: "client") else { return }
                
                let credentials = Credentials(client: client, uid: uid, accessToken: accessToken)
                success(credentials)
                
            case .failure:
                error(nil)
            }
        }
    }
}

