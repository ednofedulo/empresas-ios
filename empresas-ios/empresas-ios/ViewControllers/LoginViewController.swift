//
//  LoginViewController.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 08/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController {
    
    lazy var topImageView:UIImageView = {
        let i = UIImageView(image: UIImage(named: "top_image"))
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    lazy var logoImageView:UIImageView = {
        let i = UIImageView(image: UIImage(named: "logo_only"))
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    lazy var titleLabel:CustomLabel = {
        let l = CustomLabel(withTextStyle: .headline)
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .white
        l.text = "Seja bem vindo ao empresas!"
        l.textAlignment = .center
        return l
    }()
    
    lazy var mainStackView: UIStackView = {
        let s = UIStackView()
        s.translatesAutoresizingMaskIntoConstraints = false
        s.axis = .vertical
        s.spacing = 10
        return s
    }()
    
    lazy var loginTextField:CustomTextField = {
        let t = CustomTextField()
        t.textField.accessibilityIdentifier = "email"
        t.textField.keyboardType = .emailAddress
        t.label = "Email"
        return t
    }()
    
    lazy var passwordTextField: CustomTextField = {
        let t = CustomTextField()
        t.textField.accessibilityIdentifier = "senha"
        t.label = "Senha"
        t.textField.isSecureTextEntry = true
        return t
    }()
    
    lazy var loginButton: CustomButton = {
        let b = CustomButton()
        b.setTitle("ENTRAR", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.backgroundColor = .rosa
        b.addTarget(self, action: #selector(didTapOnLoginButton), for: .touchUpInside)
        return b
    }()
    
    weak var mainCoordinator:MainCoordinator?
    var authenticationService:AuthenticationServiceProtocol = AuthenticationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.authenticationService = AuthenticationService()
        addSubViews()
        setupConstraints()
        setupView()
    }
    
    private func addSubViews(){
        self.view.addSubview(topImageView)
        self.view.addSubview(logoImageView)
        self.view.addSubview(titleLabel)
        
        self.view.addSubview(mainStackView)
        mainStackView.addArrangedSubview(loginTextField)
        mainStackView.addArrangedSubview(passwordTextField)
        mainStackView.setCustomSpacing(20, after: passwordTextField)
        mainStackView.addArrangedSubview(loginButton)
    }
    
    private func setupConstraints(){
        
        NSLayoutConstraint.activate([
            topImageView.topAnchor.constraint(equalTo: self.view.topAnchor),
            topImageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            topImageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            topImageView.heightAnchor.constraint(equalToConstant: 200)
        ])
        
        NSLayoutConstraint.activate([
            logoImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            logoImageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 80)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -15)
        ])
        
        NSLayoutConstraint.activate([
            self.mainStackView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            self.mainStackView.centerYAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerYAnchor),
            self.mainStackView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -15)
        ])
    }
    
    private func setupView(){
        self.view.backgroundColor = .systemBackground
    }
    
    @objc func didTapOnLoginButton(){
        
        guard let email = loginTextField.value, email.isEmpty == false else {
            loginTextField.setError(with: "Email obrigatório")
            return
        }
        
        guard let password = passwordTextField.value, password.isEmpty == false else {
            passwordTextField.setError(with: "Senha obrigatória")
            return
        }
        
        self.showLoading()
        self.authenticationService.login(email: email, password: password, success: { (credentials) in
            self.hideLoading()
            guard let credentials = credentials else { return }
            self.showHomeViewController(credentials: credentials)
        }, error: { errors in
            self.hideLoading()
            guard let errors = errors, let error = errors[0] else {
                let alert = UIAlertController(title: "Erro inesperado", message: "Ocorreu algum erro inexperado. Verifique sua conexão com a internet e tente novamente.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            self.loginTextField.setError(with: "")
            self.passwordTextField.setError(with: error)
        })
    }
    
    private func showHomeViewController(credentials: Credentials) {
        guard let mainCoordinator = self.mainCoordinator else { return }
        
        mainCoordinator.credentials = credentials
        mainCoordinator.showHome()
    }
}
