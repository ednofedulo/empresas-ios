//
//  HomeViewController.swift
//  empresas-ios
//
//  Created by Edno Fedulo on 08/09/20.
//  Copyright © 2020 Edno Fedulo. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: UITableViewController {

    let enterpriseCellIdentifier = "enterpriseCell"
    var enterprises:[Enterprise]?
    var headerView:SearchHeaderView!
    weak var mainCoordinator:MainCoordinator?
    
    var authenticationService:EnterprisesServiceProtocol = EnterprisesService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews(){
        self.tableView.register(EnterpriseTableViewCell.self, forCellReuseIdentifier: enterpriseCellIdentifier)
        
        headerView = SearchHeaderView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.delegate = self
        self.tableView.tableHeaderView = headerView
        
        headerView.widthAnchor.constraint(equalTo: self.tableView.widthAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: self.tableView.topAnchor).isActive = true
        self.tableView.tableHeaderView?.layoutIfNeeded()
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        self.tableView.rowHeight = 200
        
        self.title = "Empresas"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let enterprises = self.enterprises else { return 0 }
        return enterprises.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let enterprises = enterprises {
            if enterprises.count == 0 {
                return "Nenhum registro encontrado"
            }
            
            if enterprises.count == 1 {
                return "1 registro encontrado"
            }
            
            return "\(enterprises.count) registros encontrados"
        }
        
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: enterpriseCellIdentifier, for: indexPath) as! EnterpriseTableViewCell
        guard let enterprises = self.enterprises else { return cell }
        
        let enterprise = enterprises[indexPath.row]
        cell.enterprise = enterprise
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        
        guard let enterprises = self.enterprises, enterprises.indices.contains(index) else { return }
        
        let enterprise = enterprises[index]
        let detail = DetailViewController(enterprise: enterprise)
        self.navigationController?.pushViewController(detail, animated: true)
    }

}

extension HomeViewController:SearchHeaderViewDelegate {
    func didBeginEditing() {
        UIView.animate(withDuration: 0.3) {
            self.headerView.backgroundImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        }
    }
    
    func search(searchText:String?) {
        
        guard let searchText = searchText else { return }
        
        guard let mainCoordinator = self.mainCoordinator else { return }
        guard let credentials = mainCoordinator.credentials else { return }
        
        self.showLoading(over: UIApplication.shared.windows.first)
        EnterprisesService().searchEnterprises(by: searchText, withCredentials: credentials, success: { (response) in
            self.hideLoading(from: UIApplication.shared.windows.first)
            guard let response = response else { return }
            self.enterprises = response.enterprises
            self.tableView.reloadData()
        }, error: {
            
            let alert = UIAlertController(title: "Erro inesperado", message: "Ocorreu algum erro inexperado. Verifique sua conexão com a internet e tente novamente.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            self.hideLoading()
        })
    }
}
